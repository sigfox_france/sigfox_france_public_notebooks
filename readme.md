# Sigfox France : notebooks publics

Plusieurs scripts sont fournis afin de faciliter l'utilisation des APIs Sigfox.
Aucun de ces scripts n'intègre de credential, il faut donc utiliser un compte sur le Backend Sigfox afin de créer des credentials et les utiliser dans ces outils.


# Exploiter ces notebooks

1. Vous pouvez cloner ce repository et l'exploiter avec JupyterLab en local sur votre machine ([Source](https://paul_sigfox_france@bitbucket.org/sigfox_france/sigfox_france_public_notebooks.git))

2. Vous pouvez lancer une instance grace au service Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fsigfox_france%2Fsigfox_france_public_notebooks/36ba388486d5d0c414996b47f51f9ccb6d98872c?urlpath=%2Flab%2Ftree%2Freadme.ipynb)

# Liste des notebooks

2 notebooks différents sont proposés permettant d'obtenir un niveau simulé et géocoder une adresse.

## coverage_simulation

Permet d'évaluer la couverture disponible en une coordoonée latitude/longitude.
Les adresses doivent être fournies dans un fichier au format xlsx.

## geocoding

Permet de transformer une adresse en coordonnées latitude/longitude.
Ce notebook utilise l'API de Here dont la clé accessible gratuitement permettra le geocoding de 250 000 adresses par mois.


# Ajouter ses credentials et ses données

Ouvrir le notebook souhaité et modifier les parties suivantes

## credentials : Les credentials sont liés aux droits de votre compte Backend Sigfox

* login = 'ajouter_ici_le_login'
* password = 'ajouter_ici_le_mot_de_passe'

## Inputs : Il faut pour la majorité des notebooks utiliser un fichier sources avec les adresses ainsi que les en-têtes à interpréter

* addr = 'chemin_fichier_source.xlsx'
* sheetname = 'nom_feuille_excel'
* latheader = 'nom_entete_latitude'
* longheader = 'nom_entete_longitude'

## Lancer une analyse

* Suivez les étapes ci-dessus
* Cliquer sur ► sur le bandeau supérieur pour lancer la cellule active